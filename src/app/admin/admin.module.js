/**
 * Created by Job on 6/27/2016.
 */
(function () {
    'use strict';

    angular.module('app.admin', ['app.admin.core', 'app.admin.home','app.admin.tender','app.admin.vacancy','app.admin.user','app.admin.news'
    ]);
})();
