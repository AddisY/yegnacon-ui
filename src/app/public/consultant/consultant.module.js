/**
 * Created by Yididya on 17/07/2016.
 */
(function () {
    'use strict';

    angular.module('app.public.consultant', ['app.public.consultant.vacancy', 'app.public.consultant.tender', 'app.public.consultant.project']);
})();
