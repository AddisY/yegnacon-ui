/**
 * Created by Yididya on 17/07/2016.
 */
(function () {
    'use strict';

    angular.module('app.public.contractor', ['app.public.contractor.vacancy', 'app.public.contractor.tender', 'app.public.contractor.project','app.public.contractor.employee']);
})();
