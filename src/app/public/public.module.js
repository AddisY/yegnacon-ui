/**
 * Created by Job on 6/27/2016.
 */
(function() {
    'use strict';

    angular.module('app.public', ['app.public.core', 'app.public.home','app.public.profile', 'app.public.proforma','app.public.tender',
        'app.public.vacancy' ,'app.public.project','app.public.product','app.public.professional','app.public.supplier',
        'app.public.consultant','app.public.contractor', 'app.public.projectOwner', 'app.public.notification','app.public.documentation','app.public.employee']);

})();
