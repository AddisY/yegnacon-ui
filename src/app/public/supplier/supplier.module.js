/**
 * Created by Yididya on 17/07/2016.
 */
(function () {
    'use strict';

    angular.module('app.public.supplier', ['ngMessages', 'app.public.supplier.vacancy', 'app.public.supplier.tender']);
})();
